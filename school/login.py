from tkinter import *

login_window = Tk()
login_window.title("Login")
login_window.geometry("325x400")

user_database = {
	"magnus": "10DDAB35E7A7215CE644F2B7A3EFC985",
	"thea": "F591BF1B4DECB98D39089F5D66261E07",
	"paal": "ED8BB585FCD9DE5DC9FDEE135880C6DC"
}

blank = Label(login_window, text="                               ")

def user_login():
	"""Function for login users into the application
	
	Paramaters
	----------
	
	str

		This allowes the user to enter their credentials 
		that will be checked towards a database
	
	Returns
	-------
	
	str

		Allows the user to open a connection to the server WIP
	
	"""

	username = Label(login_window, text="Please enter Username")
	password = Label(login_window, text="Enter your password")
	username_window = Entry(login_window, width=16, borderwidth=1, fg="black", bg="white", font='Times 8 bold')
	username_window.insert(0,"Username")
	password_window = Entry(login_window, width=16, borderwidth=1, fg="black", bg="white", font='Times 8 bold', show="*")
	password_window.insert(0, "Password")
	username.grid(row=0, column=1)
	username_window.grid(row=1, column=1, columnspan=4, padx=10, pady=10, ipady=7)
	password.grid(row=2, column=1)
	password_window.grid(row=3, column=1, columnspan=4, padx=10, pady=10, ipady=7)
	return


# Login = password.Entry(login_window)

blank.grid(row=0, column=0)
user_login()



# password.grid(row=1, column=1)

# show = "*"


login_window.mainloop()
